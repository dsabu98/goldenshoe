import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WomensProductsComponent } from './womens-products.component';

describe('WomensProductsComponent', () => {
  let component: WomensProductsComponent;
  let fixture: ComponentFixture<WomensProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WomensProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WomensProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
