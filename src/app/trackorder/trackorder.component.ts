import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface OrderDetails {

}


@Component({
  selector: 'app-trackorder',
  templateUrl: './trackorder.component.html',
  styleUrls: ['./trackorder.component.css']
})
export class TrackorderComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(TrackorderComponentDialog, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog Closed');
    })
  }

  ngOnInit(): void {

  }
}

@Component({
  selector: 'trackorder-component-dialog',
  templateUrl: './trackorder-component-dialog.html',
  styleUrls: ['./trackorder.component.css']
})
export class TrackorderComponentDialog {
  
  constructor(
    public dialogRef: MatDialogRef<TrackorderComponentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: OrderDetails) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}