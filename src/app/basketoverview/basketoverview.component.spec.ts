import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketoverviewComponent } from './basketoverview.component';

describe('BasketoverviewComponent', () => {
  let component: BasketoverviewComponent;
  let fixture: ComponentFixture<BasketoverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasketoverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketoverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
