import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { BasketServiceService } from '../services/basket-service.service';

@Component({
  selector: 'app-basketoverview',
  templateUrl: './basketoverview.component.html',
  styleUrls: ['./basketoverview.component.css']
})
export class BasketoverviewComponent implements OnInit {

  constructor(private basket: BasketServiceService) { }

  itemsInBasket: number;
  shoePrice: number
  totalPrice: number;
  empty: Boolean;
  discountState: Boolean;
  discount: String;
  finalPrice: number;
  
  

  ngOnInit(): void {
    this.basketStatus()
    this.itemsInBasket = this.basket.getBaseketItems();
    this.calculatePrice();
    this.discountState = false;
    this.finalPrice = this.totalPrice;
  }

  basketStatus() {
    if (this.basket.getBaseketItems() == 0) {
      this.empty = true;
      return this.empty
    }
    else {
      this.empty = false;
      return this.empty;
    }
  }

  discountStatus() {
      return this.discountState;
  }

  calculatePrice() {
    this.shoePrice = 59.00;
    this.totalPrice = 0;
    this.totalPrice = this.itemsInBasket * this.shoePrice;
    return this.totalPrice;
  }

  addDiscount() {
    this.discountState = true;
    this.discount = "Discount successfully applied.";
    this.finalPrice = this.totalPrice - (this.totalPrice * 0.2);
  }

  calculateFinalPrice() {
    if (this.discountState == false) {
      this.finalPrice = this.totalPrice;
      return this.finalPrice;
    }
    else {
      this.finalPrice = this.totalPrice - (this.totalPrice * 0.2);
      return this.finalPrice;
    }
  }

}
