import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { BasketServiceService } from '../services/basket-service.service';

export interface SizeDetails {

}

@Component({
  selector: 'app-shoe',
  templateUrl: './shoe.component.html',
  styleUrls: ['./shoe.component.css']
})
export class ShoeComponent implements OnInit {

  constructor(public dialog: MatDialog, private basket: BasketServiceService) { }
  ngOnInit(): void {
  }
  openSizeGuide(): void {
    const dialogRef = this.dialog.open(SizeGuideDialog, {
      width: '70%',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog Closed');
    })
  }

  addToBasket() {
    this.basket.addBasketItem();
    console.log(this.basket.getBaseketItems());
  }
}

@Component({
  selector: 'sizeguide-dialog',
  templateUrl: './sizeguide.html',
  styleUrls: ['./shoe.component.css']
})
export class SizeGuideDialog {
  constructor(
    public dialogRef: MatDialogRef<SizeGuideDialog>,
    @Inject(MAT_DIALOG_DATA) public data: SizeDetails) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}