import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card'
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppComponent } from './app.component';
import { ReturnsComponent } from './returns/returns.component';
import { TrackorderComponent } from './trackorder/trackorder.component';
import { HomeComponent } from './home/home.component';
import { WomensProductsComponent } from './womens-products/womens-products.component';
import { ShoeComponent } from './shoe/shoe.component';
import { OutofstockComponent } from './outofstock/outofstock.component';
import { BasketoverviewComponent } from './basketoverview/basketoverview.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'returns', component: ReturnsComponent },
  { path: 'trackorder', component: TrackorderComponent },
  { path: 'womens', component: WomensProductsComponent },
  { path: 'shoe', component: ShoeComponent }, 
  { path: 'outofstock', component: OutofstockComponent },
  { path: 'basketoverview', component: BasketoverviewComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatFormFieldModule,
    MatTabsModule,
    MatDialogModule,
    MatChipsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSnackBarModule,
    FormsModule,
    RouterModule.forRoot( appRoutes, { enableTracing: true } ),
  ],
  declarations: [
    AppComponent,
    ReturnsComponent,
    TrackorderComponent,
    HomeComponent,
    WomensProductsComponent,
    ShoeComponent,
    OutofstockComponent,
    BasketoverviewComponent,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }