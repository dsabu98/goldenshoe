import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BasketServiceService {

  basketItems: number;

  constructor() {
    this.basketItems = 0;
  }

  getBaseketItems() {
    return this.basketItems;
  }

  addBasketItem() {
    localStorage.setItem('itemsInBasket', JSON.parse(this.getBaseketItems().toString()));
    return this.basketItems += 1; 
  }

}
