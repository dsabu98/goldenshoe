import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface NotifyMeDetails {

}

@Component({
  selector: 'app-outofstock',
  templateUrl: './outofstock.component.html',
  styleUrls: ['./outofstock.component.css']
})
export class OutofstockComponent implements OnInit {

  constructor(public dialog: MatDialog) { }
  notifyMe(): void {
    const dialogRef = this.dialog.open(NotifyMeDialog, {
      width: '50%',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog Closed');
    })
  }

  ngOnInit(): void {
  }
}

@Component({
  selector: 'notifyme-dialog',
  templateUrl: './notifyme.html',
  styleUrls: ['./outofstock.component.css']
})
export class NotifyMeDialog {
  success: string;

  constructor(
    public dialogref: MatDialogRef<NotifyMeDetails>,
    @Inject(MAT_DIALOG_DATA) public data: NotifyMeDetails) {}

    onNoClick(): void {
      this.dialogref.close();
    }
  
    onClick() {
      this.success = "An email will be sent as soon as the item is back in stock.";
      return this.success;
    }
  
  
}