import { Component } from '@angular/core';
import { BasketServiceService } from '../app/services/basket-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Golden Shoe';
  basketItems: Number;

  constructor(private basket: BasketServiceService){
    this.basketItems = this.basket.getBaseketItems();
  }

  ngOnInit(): void {
    setTimeout(() => { this.ngOnInit() }, 1 * 10);
    this.basketItems = this.basket.getBaseketItems();
    // this.basketItems = parseInt(localStorage.getItem('itemsInBasket'));
  }

  ngOnDestroy(): void {
    localStorage.clear();
  }
}
